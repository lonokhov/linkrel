{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MultiWayIf   #-}
module Main (main) where

import           Control.Exception  (SomeException, catch, onException, throwIO,
                                     try)
import           Control.Monad      (unless)
import           Data.Char          (isDigit)
import           Data.Coerce        (coerce)
import           Data.Foldable      (for_)
import           Data.Function      (fix)
import           Data.List          (isPrefixOf, isSuffixOf, stripPrefix)
import           Data.Semigroup     (Max (..), (<>))
import           System.Directory   (copyFile, createDirectory, listDirectory)
import           System.Environment (getArgs)
import           System.Exit        (exitFailure)
import           System.FilePath    (dropTrailingPathSeparator, splitFileName,
                                     (<.>), (</>))
import           System.IO.Error    (isAlreadyExistsError, isDoesNotExistError)
import           System.Posix       (createSymbolicLink, getFileStatus,
                                     getSymbolicLinkStatus, isDirectory,
                                     isRegularFile, isSymbolicLink,
                                     readSymbolicLink, removeLink, rename)
import           Text.Read          (readMaybe)

checkTarget :: FilePath -> IO ()
checkTarget fp = do
  status <- (Just <$> getSymbolicLinkStatus fp)
    `catch` \e -> if isDoesNotExistError e
                  then pure Nothing
                  else throwIO e
  case status of
    Nothing -> pure ()
    Just st ->
      unless (isSymbolicLink st) $
        putStrLn "Target is not a symbolic link"

checkSource :: FilePath -> IO ()
checkSource fp = do
  status <- getFileStatus fp
    `catch` \e ->
       putStrLn ("Error reading source " <> fp <> ": "
                 <> show (e:: SomeException))
       *> exitFailure
  unless (isDirectory status) $
    putStrLn $ "source " <> fp <> " is not a directory"


findNextTargetDir :: FilePath -> IO FilePath
findNextTargetDir target =
    mkTargetDir
    . maybe 0 (succ . getMax)
    . foldr maxTarget Nothing
    . filter isLink
    <$> listDirectory dir
  where
    isLink fp =
       isPrefixOf name fp && isSuffixOf "-link" fp
    (dir, name) = splitFileName (dropTrailingPathSeparator target)

    nameSuf = name <> "-"
    maxTarget :: FilePath -> Maybe (Max Int) -> Maybe (Max Int)
    maxTarget nm !acc = acc <> coerce num
      where
        num :: Maybe Int
        num = do
            stripped <- stripPrefix nameSuf nm
            case span isDigit stripped of
              ([], _)       -> Nothing
              (xs, "-link") -> readMaybe xs
              _             -> Nothing
    mkTargetDir i = dir </> nameSuf <> show i <> "-link"

copyPath :: FilePath -> FilePath -> IO ()
copyPath f t = do
  --putStrLn $ "copy " <> f <> " -> " <> t
  fstat <- getSymbolicLinkStatus f
  if | isRegularFile fstat ->  copyFile f t
     | isSymbolicLink fstat -> do
        tgt <- readSymbolicLink f
        createSymbolicLink tgt t
     | isDirectory fstat -> do
         createDirectory t
         items <- listDirectory f
         for_ items $ \nm ->
           copyPath (f </> nm) (t</>nm)
     | otherwise ->
         putStrLn $ "Skipping " <> f <> " not a file/directory/symbolic link"


copyToTarget :: FilePath -> FilePath -> IO FilePath
copyToTarget src target = do
  dir <- flip fix (0::Int) $ \next iter -> do
    dir <- findNextTargetDir target
    r <- try $ createDirectory dir
    case r of
      Left e -> if isAlreadyExistsError e && iter < 5
                then next (succ iter)
                else throwIO e
      Right _ -> pure dir
  items <- listDirectory src
  for_ items $ \nm -> do
    let from = src </> nm
        to = dir </> nm
    putStrLn $ "Copying files from " <> from <> " to " <> to
    copyPath from to
  pure dir


perform :: FilePath -> FilePath -> IO ()
perform src tgt = do
  checkSource src
  checkTarget tgt
  dir <- copyToTarget src tgt
  putStrLn $ "Linking " <> dir <> " -> " <> tgt
  let tmp = dir <.> "lnk"
  createSymbolicLink dir tmp
  rename tmp tgt `onException` removeLink tmp


main :: IO ()
main = do
  args <- getArgs
  (source, target) <- case args of
    [source, target] -> pure (source, target)
    _                -> do
      putStrLn "usage: linkrel source target-link"
      exitFailure
  perform source target
