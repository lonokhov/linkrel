{ pkgs ? import <nixpkgs> {} }:
let
  pkg = pkgs.haskellPackages.callCabal2nix "linkrel" ./. {};
in pkg
